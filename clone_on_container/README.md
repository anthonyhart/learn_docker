# Clone On Container

Clone On Container is a docker demo that clone and build the golang binary on the container.
All you need is only the Dockerfile.

Leaked SECRET file on docker image ? SSH Private KEY ? We can solve it by using **Multi-stage Builds** When working with multi-stage builds, you are building multiple 
Docker images in a single Dockerfile, but only the last one is the real result. 
The other ones are there to support it, and don’t leave any traces behind in the 
final image to be built.

This is really convenient for handling secrets! You simply provide your private SSH key
to one of the intermediate images, use it to install dependencies, download the data 
or clone a Git repository, and pass directories containing that data into your final 
image build process, while leaving the secret credentials safe and sound in the 
intermediate image.

More explanation :
https://vsupalov.com/build-docker-image-clone-private-repo-ssh-key/

## Getting Started

These instructions will get you a copy of the project up and running on your 
local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

Docker
```
https://docs.docker.com/docker-for-mac/install/
```

### Installing

Get the  Dockerfile

```
$ cd $GOPATH/src
$ mkdir -p bitbucket.org/anthonyhart/learn_docker
$ git clone git@bitbucket.org:anthonyhart/learn_docker.git
$ cd learn_docker/clone_on_container
```

## Run

Create docker image with arguments of your local SSH private key

```
$ docker build -t coc-image --build-arg SSH_PRIVATE_KEY="$(cat ~/.ssh/id_rsa)" .
```

Check docker image

```
$ docker images
```

Run docker container

```
$ docker -it --rm --name bol-instance -p 8081:8081 coc-image
```
Let's break down the above command to see what it does. The docker run command is used to run a container from an image

- The -it flag starts the container in an interactive mode,
- The --rm flag cleans out the container after it shuts down,
- The --name bol-instance names the container bol-instance,
- The -p 8080:8081 flag allows the container to be accessed at port 8080,

Try it

```
$ curl -X GET http://localhost:8080 
```

It should be returning success json

## Built With

* Go (https://golang.org/) - open source programming language that makes it easy to build simple, reliable, 
and efficient software

## Authors

* **Anthony Hartanto** - *Initial work* - [ahartanto](https://github.com/ahartanto)