package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

// HealthHandlerResponse response for health handler
type HealthHandlerResponse struct {
	Message string `json:"message"`
}

// HealthHandler handle health request
func HealthHandler(w http.ResponseWriter, req *http.Request) {
	body := HealthHandlerResponse{
		Message: "Success",
	}
	respBody, _ := json.Marshal(body)

	w.Write(respBody)
	w.WriteHeader(http.StatusOK)
	return
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", HealthHandler)
	http.ListenAndServe(":8081", r)
}
