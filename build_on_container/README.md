# Build on Container

Build on container is a demonstration of docker deployment that copy the local project
to the container then download project dependencies and build the golang binary on the 
container. The demo is already using multistage build to reduce the image file.

## Getting Started

These instructions will get you a copy of the project up and running on your 
container machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

Docker
```
https://docs.docker.com/docker-for-mac/install/
```

### Installing

Clone the project to your directory :

```
$ cd $GOPATH/src
$ mkdir -p bitbucket.org/anthonyhart/learn_docker
$ git clone git@bitbucket.org:anthonyhart/learn_docker.git
$ cd learn_docker/build_on_container
```

## Run

Create docker image

```
$ docker build -t boc-image .
```

Check docker image

```
$ docker images
```

Run docker container

```
$ docker -it --rm --name boc-instance -p 8081:8081 boc-image
```
Let's break down the above command to see what it does. The docker run command is used to run a container from an image

- The -it flag starts the container in an interactive mode,
- The --rm flag cleans out the container after it shuts down,
- The --name boc-instance names the container boc-instance,
- The -p 8080:8081 flag allows the container to be accessed at port 8080,

Try it

```
$ curl -X GET http://localhost:8080 
```

It should be returning success json


## Built With

* Go (https://golang.org/) - open source programming language that makes it easy to build simple, reliable, 
and efficient software

## Authors

* **Anthony Hartanto** - *Initial work* - [ahartanto](https://github.com/ahartanto)