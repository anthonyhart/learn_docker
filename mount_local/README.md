# Mount Local

Mount Local is a demonstration of docker deployment that mount the local volume
to the container.

## Getting Started

These instructions will get you a copy of the project up and running on your 
container machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

Docker
```
https://docs.docker.com/docker-for-mac/install/
```

Dep
```
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
```

### Installing

Clone the project to your directory :

```
$ cd $GOPATH/src
$ mkdir -p bitbucket.org/anthonyhart/learn_docker
$ git clone git@bitbucket.org:anthonyhart/learn_docker.git
$ cd learn_docker/mount_local
```

Get project dependencies

```
$ dep ensure -v
```

Build binary for linux

```
$ CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
```

## Run

Create docker image

```
$ docker build -t ml-image .
```

Check docker image

```
$ docker images
```

Run docker container

```
$ docker run -it --rm -p 8080:8081 --name ml-instance -v /Users/anthony/go/src/bitbucket.org/anthonyhart/learn_docker/mount_local:/opt/mount_local ml-image
```
Let's break down the above command to see what it does. The docker run command is used to run a container from an image

- The -it flag starts the container in an interactive mode,
- The --rm flag cleans out the container after it shuts down,
- The --name boc-instance names the container boc-instance,
- The --v mount the local volume to the container volume
- The -
- The -p 8080:8081 flag allows the container to be accessed at port 8080,

Try it

```
$ curl -X GET http://localhost:8080 
```

It should be returning success json


## Built With

* Go (https://golang.org/) - open source programming language that makes it easy to build simple, reliable, 
and efficient software

## Authors

* **Anthony Hartanto** - *Initial work* - [ahartanto](https://github.com/ahartanto)