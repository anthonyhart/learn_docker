![picture](https://www.docker.com/sites/default/files/vertical.png)

# Learn Docker

Learn Docker is a docker demo that demonstrate golang project deployment using docker. Each
folder contain standalone application that demonstrate docker deployment with your preference
method. There are several method for golang project deployment :

- **Build on Container** : Copy the local project to the container then download the 
dependencies and build the binary on container. 
Please look at [build_on_container](https://bitbucket.org/anthonyhart/learn_docker/src/master/build_on_container/)
for example.
- **Clone on Container** : Clone the project & get dependencies on the container by using
  RUN git clone ... in a Dockerfile and build the image each time the source code 
  changes. Please look at [clone_on_container](https://bitbucket.org/anthonyhart/learn_docker/src/master/clone_on_container/)
  for example.
- **Mount Local** : Run the binary on the container by mounting local volume by
  using docker run -v $(pwd):/whatever. Please look at 
  [mount_local](https://bitbucket.org/anthonyhart/learn_docker/src/master/mount_local/)
  for example.

## Getting Started

There are several method of docker deployment method that you may use which the most
suitable method within your squad or environment. Each of it have pros and cons, i cant
tell you which one is the best, but you can find which one more suit to your needs.

#### Not using Docker
* PROS 

    - No need to learn docker

* CONS
    - Need to install all of the dependencies


#### Build on Container
* PROS 

    - What you deployed will be the same as your local
    - No need to think about leaked secret file that copied to docker images

* CONS
    - Multi User Deployment ? get confused which code is deployed on the container


#### Clone on Container

* PROS 
    - Clear of which code get deployed, it's specific branch / tag , not your own local
    - Need only Dockerfile

* CONS
    - Need to care about ssh key that copied on the images
    
#### Mount Volume

* PROS 
    - No need to think, just used docker to serve ur local project

* CONS
    - Multiple user ?
    - etc

## Build With

* Go (https://golang.org/) - open source programming language that makes it easy to build simple, reliable, 
and efficient software

## Authors

* **Anthony Hartanto** - *Initial work* - [ahartanto](https://github.com/ahartanto)